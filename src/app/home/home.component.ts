import { Component, OnInit, HostListener } from '@angular/core';
import { SettingsService } from '../commons/settings.service';
import { ActivatedRoute } from '@angular/router';
import { CloudMessagingService } from '../commons/cloud-messaging.service';
import { SnotifyService } from 'ng-snotify';
import { MatDialog } from '@angular/material';
import { LoginComponent } from './login/login.component';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  lang:string;
  heightWindows = 500;
  heightborder=170;

  constructor(
    public settingsService: SettingsService,
    private _cloudMessagingService:CloudMessagingService,
    private _activatedRoute: ActivatedRoute,
    private snotifyService: SnotifyService,
    public dialog: MatDialog,
    public auth:AngularFireAuth
  ) {
    this._cloudMessagingService.requestPermission();
   }

  ngOnInit() {
    console.log('initApp');
		this.lang = this._activatedRoute.snapshot.params['lang'];
    this.settingsService.loadLabelsByLang(this.lang);
    this.heightWindows = window.innerHeight;
    this._cloudMessagingService.listen();
  }

  @HostListener('window:resize', [ '$event' ])
	onResize(event) {
		this.heightWindows = event.target.innerHeight;
  }
  
  showSignIn(){
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '300px',
      data:{}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.auth.authState.subscribe((user) => {
        this.settingsService.user = user;
      });
    });
  }

  signOut(){
    this.settingsService.signOut();
  }
}
