import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SettingsService } from '../../commons/settings.service';
import { MatButton } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

/*
  email: string = 'adminapp@fosil.cz';
  password: string = '4dm1nf0s1l';
*/
  email: string = '';
  password: string = '';

  constructor(public settingsService: SettingsService) { }

  ngOnInit() {
  }

  signIn() {
    this.settingsService.signIn(this.email, this.password);
  }
}
