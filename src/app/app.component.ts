import { Component, OnInit } from '@angular/core';
import { LabelsService } from './commons/labels.service';
import { SettingsService } from './commons/settings.service';
import { Router, ActivatedRoute } from '../../node_modules/@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {
	title = 'fosil-pwa';

	lang: string;
	orderId: string;

	constructor(public settingsService: SettingsService, private activatedRoute: ActivatedRoute) {
		console.log('initApp');
		this.lang = this.activatedRoute.snapshot.params['lang'];
		this.orderId = this.activatedRoute.snapshot.params['orderId'];
		console.log(this.lang);
		if (this.lang === undefined) {
			this.lang = 'cs-CZ';
		}
		this.settingsService.selectedLang = this.lang;
		this.settingsService.loadAvailableLang();
		this.settingsService.loadLabelsByLang(this.lang);
	}

	ngOnInit() {}
}
