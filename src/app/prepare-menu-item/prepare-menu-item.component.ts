import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../commons/settings.service';
import { GroupIngredientService } from '../menu/group-ingredient.service';
import { GroupIngredient } from '../entity/group-ingredient';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { MenuItem } from '../entity/menu-item';
import { OrderService } from '../menu/order.service';
import { Ingredient } from '../entity/ingredient';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-prepare-menu-item',
	templateUrl: './prepare-menu-item.component.html',
	styleUrls: [ './prepare-menu-item.component.scss' ]
})
export class PrepareMenuItemComponent implements OnInit {
	lang: string;
	orderId: string;

	groups: Observable<GroupIngredient[]>;

	constructor(
		public settingsService: SettingsService,
		private _groupIngredientService: GroupIngredientService,
		private _orderService: OrderService,
		private route: Router,
		private activatedRoute: ActivatedRoute
	) {
		console.log('initApp');
		this.lang = this.activatedRoute.snapshot.params['lang'];
		this.orderId = this.activatedRoute.snapshot.params['orderId'];
		this.settingsService.loadLabelsByLang(this.lang);
	}

	ngOnInit() {
		this. groups = this._groupIngredientService.getGroups();
	}

	back() {
		this.route.navigate([ this.lang + '/' + this.orderId + '' ]);
	}

	addMenuItemToOrder() {
		const item = new MenuItem();
		item.id = this._orderService.getId();
		item.orderId = this.orderId;
		item.label = 'menu.item.burrito';
		item.amount = 0;
		(item.details = 'ninguno'),
			this._orderService
				.upsertMenuItem(item)
				.then((success) => {
					console.log('Item added');
					this.back();
				})
				.catch((e) => {
					console.log(e);
				});
	}

	selectIngredient(ingredient:Ingredient){
		if(ingredient.isSelected){
			ingredient.isSelected = false;
		} else {
			ingredient.isSelected = true;
		}
	}
}
