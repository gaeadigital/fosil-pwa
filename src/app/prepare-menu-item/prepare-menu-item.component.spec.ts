import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepareMenuItemComponent } from './prepare-menu-item.component';

describe('PrepareMenuItemComponent', () => {
  let component: PrepareMenuItemComponent;
  let fixture: ComponentFixture<PrepareMenuItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepareMenuItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepareMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
