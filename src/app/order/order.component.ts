import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { GroupIngredientService } from '../menu/group-ingredient.service';
import { SettingsService } from '../commons/settings.service';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { Order } from '../entity/order';
import { OrderService } from '../menu/order.service';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Utils } from '../utils/utils';


@Component({
	selector: 'app-order',
	templateUrl: './order.component.html',
	styleUrls: [ './order.component.scss' ],
	providers: [ OrderService ]
})
export class OrderComponent implements OnInit {

	@ViewChild('audioOption',null) audioPlayerRef: ElementRef;
	
	lang: string;
	orderId: string;

	isSmallScreen:boolean;

  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];

	displayedColumns: string[] = [ 'receivedDate','address','phoneNumber','order','total' ];
	dataSource = new MatTableDataSource<Order>();
	dataSourceKitchen = new MatTableDataSource<Order>();
	dataSourceReady2Send = new MatTableDataSource<Order>();
	dataSourceSended = new MatTableDataSource<Order>();
	dataSourceDelivered = new MatTableDataSource<Order>();

	totalNewOrders:number;

	listOrder = new Array<Order>();
	listOrderKitchen = new Array<Order>();
	listOrderReady = new Array<Order>();
	listOrderSended = new Array<Order>();
	listOrderDelivered = new Array<Order>();
	
	@ViewChild('newPaginator', { read: MatPaginator, static: true })
	paginator: MatPaginator;
	@ViewChild('kitchenPaginator', {read: MatPaginator,  static: true })
	paginatorKitchen: MatPaginator;
	@ViewChild('readyPaginator', { read: MatPaginator, static: true })
	readyPaginator: MatPaginator;
	@ViewChild('sendedPaginator', { read: MatPaginator, static: true })
	paginatorSended: MatPaginator;
	@ViewChild('deliveredPaginator', {read: MatPaginator,  static: true })
	paginatorDelivered: MatPaginator;

	// Filter orders
	dayOrders: Date;

	totalDelivered:number;
	

	constructor(
		public settingsService: SettingsService,
		private _groupIngredientService: GroupIngredientService,
		private _route: Router,
		public dialog: MatDialog,
		private _activatedRoute: ActivatedRoute,
		private _orderService: OrderService,
		private breakpointObserver: BreakpointObserver
	) {
		console.log('initApp');
		this.lang = this._activatedRoute.snapshot.params['lang'];
		this.settingsService.loadLabelsByLang(this.lang);
		this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
		this.totalNewOrders=0;
		this.dayOrders = new Date();
		this.totalDelivered = 0;
	}

	@HostListener('window:resize', [ '$event' ])
	onResize(event) {
		this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
	}

	ngOnInit() {
		const start = Utils.fixStartDate(new Date(this.dayOrders));
		const end = Utils.fixEndDate(Utils.calculateNextDate(new Date(this.dayOrders),1,'D'));
		console.log(start+" - "+end);
		this._orderService.getOrdersReceivedNStatus('NEW',start,end).subscribe(ordersSub => {
			this.listOrder = new Array<Order>();
			ordersSub.forEach(orderDoc => {
				this.listOrder.push(orderDoc);
			});
			if(this.totalNewOrders < ordersSub.length){
				this.audioPlayerRef.nativeElement.play();
				this.totalNewOrders = ordersSub.length;
			} 
			this.totalNewOrders = ordersSub.length;
			this.dataSource = new MatTableDataSource<Order>(ordersSub);
			this.dataSource.paginator = this.paginator;
			
		});
		this._orderService.getOrdersReceivedNStatus('KITCHEN',start,end).subscribe(ordersSub => {
			this.listOrderKitchen = new Array<Order>();
			ordersSub.forEach(orderDoc => {
				this.listOrderKitchen.push(orderDoc);
			});
			this.dataSourceKitchen = new MatTableDataSource<Order>(ordersSub);
			this.dataSourceKitchen.paginator = this.paginatorKitchen;
		});
		this._orderService.getOrdersReceivedNStatus('READY',start,end).subscribe(ordersSub => {
			this.listOrderReady = new Array<Order>();
			ordersSub.forEach(orderDoc => {
				this.listOrderReady.push(orderDoc);
			});
			this.dataSourceReady2Send = new MatTableDataSource<Order>(ordersSub);
			this.dataSourceReady2Send.paginator = this.readyPaginator;
		});
		this._orderService.getOrdersReceivedNStatus('SENDED',start,end).subscribe(ordersSub => {
			this.listOrderSended = new Array<Order>();
			ordersSub.forEach(orderDoc => {
				this.listOrderSended.push(orderDoc);
			});
			this.dataSourceSended = new MatTableDataSource<Order>(ordersSub);
			this.dataSourceSended.paginator = this.paginatorSended;
		});
		this._orderService.getOrdersReceivedNStatus('DELIVERED',start,end).subscribe(ordersSub => {
			this.listOrderDelivered = new Array<Order>();
			ordersSub.forEach(orderDoc => {
				this.totalDelivered += (orderDoc.deliveryAmount+orderDoc.totalAmount);
				this.listOrderDelivered.push(orderDoc);
			});
			this.dataSourceDelivered = new MatTableDataSource<Order>(ordersSub);
			this.dataSourceDelivered.paginator = this.paginatorDelivered;
		});
		
	}

	changeOrderDate(){
		console.log(this.dayOrders);
		this.totalDelivered = 0;
		this.ngOnInit();
	}

	showDetail(order:Order){
				const dialogRef = this.dialog.open(OrderDetailComponent, {
				data: {
					order:order
				},
				width: (this.isSmallScreen)?'95%':'50%'
			});
	
			dialogRef.afterClosed().subscribe((result) => {
				console.log('The dialog was closed');
			});
		}

	callNumber(number){
		window.location.href='tel:'+number;
	}
}
