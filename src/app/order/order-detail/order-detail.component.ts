import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Order } from '../../entity/order';
import { SettingsService } from '../../commons/settings.service';
import { OrderService } from '../../menu/order.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  order: Order;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public settingsService: SettingsService,
    private _orderService: OrderService) { }

  ngOnInit() {
    if (this.data) {
      this.order = this.data.order;
      if(!this.order.isCanceled){
        this.order.isCanceled = false;
      }
    } else {
      this.order = new Order();
    }

  }

  moveStatus(status: string) {
    this.order.status = status;
    if(status === 'DELIVERED'){
      this.order.address = null;
      this.order.phoneNumber = null;
      this._orderService.upsertOrderDelivered(this.order).then(success => {
        console.log('Success');
      }).catch(err => {
        console.log(err);
      });
    } else {
      this._orderService.upsertOrderStatus(this.order).then(success => {
        console.log('Success');
      }).catch(err => {
        console.log(err);
      });
    }
    
  
  }

  toggleCancel(){
    if(this.order.isCanceled){
      this.order.isCanceled = false;
    } else {
      this.order.isCanceled = true;
    }
  }
}
