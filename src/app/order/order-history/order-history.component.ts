import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SettingsService } from 'src/app/commons/settings.service';
import { Order } from 'src/app/entity/order';
import { OrderHistory } from 'src/app/entity/order-history';
import { OrderService } from 'src/app/menu/order.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {

  order: Order;
  orderHistory:OrderHistory;

  listHistory:Array<OrderHistory>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public settingsService: SettingsService,
  private _orderService: OrderService) { }

ngOnInit() {
  this.orderHistory = new OrderHistory();
  this.listHistory = new Array<OrderHistory>();
  if (this.data) {
    this.order = this.data.order;
    this.orderHistory.orderId = this.order.id;

    this._orderService.getHistoryOrder(this.order.id).then(result => {
      result.forEach(element=> {
        this.listHistory = element;
      })
    });

    if(!this.order.isCanceled){
      this.order.isCanceled = false;
    }
  } else {
    this.order = new Order();
  }


}

sendInfo(){
  this.orderHistory.recordDate =new Date();
  console.log(this.orderHistory);
  this._orderService.upsertHistory(this.orderHistory).then(result => {
    this.orderHistory = new OrderHistory();
    this.orderHistory.orderId = this.order.id;

  });
}

}
