import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Label } from '../entity/label';

@Injectable({
  providedIn: 'root'
})
export class LabelsService {

  labelsCollectionName = 'labels';

  constructor(private afs: AngularFirestore) { }

  async getLabels()  {
    return await this.afs.collection(this.labelsCollectionName).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Label;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }

  upsertLabel(labelId, label){
    
    return this.afs.collection(this.labelsCollectionName).doc(labelId).set(label,{merge:true});
  }

  deleteLabel(labelId){
    return this.afs.collection(this.labelsCollectionName).doc(labelId).delete();
  }
}
