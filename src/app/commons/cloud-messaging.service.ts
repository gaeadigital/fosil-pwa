import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CloudMessagingService {

  messagingCollectionName = 'messaging';

  constructor(private afMessaging: AngularFireMessaging, private afs:AngularFirestore) { }

  requestPermission() {
    this.afMessaging.requestPermission
      .pipe(mergeMapTo(this.afMessaging.tokenChanges))
      .subscribe(
        (token) => { 
          console.log('Permission granted! Save to the server!', token);
          this.saveToken(token);
        },
        (error) => { console.error(error); },  
      );
  }

  listen() {
    return this.afMessaging.messages.subscribe((message) => { console.log(message); });
  }

  saveToken(token){
    const id = this.afs.createId();

    const data = {
      'fcmToken':token
    };

    this.afs.doc(this.messagingCollectionName+'/'+id).set(data,{merge:true});
  }
}
