import { Injectable } from '@angular/core';
import { Lang } from '../entity/lang';
import { LabelsService } from './labels.service';
import { LangService } from './lang.service';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { SnotifyService } from 'ng-snotify';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Setting } from '../entity/setting';
import { DistanceAmount } from '../entity/distance-amount';

@Injectable({
	providedIn: 'root'
})
export class SettingsService {
	selectedLang: string;
	actualOrder: string;
	openSideMenu: boolean = false;

	labels = [];
	langs: Array<Lang>;

	user: firebase.User = null;

	settingsCollectionName = 'settings';
	distanceCollectionName = 'distances';

	constructor(private _labelsService: LabelsService, private _langService: LangService,
		private _route: Router, private snotifyService: SnotifyService,
		private _activatedRoute: ActivatedRoute, private auth: AngularFireAuth, private afs: AngularFirestore) {
		this.selectedLang = this._activatedRoute.snapshot.params['lang'];
		this.actualOrder = this._activatedRoute.snapshot.params['orderId'];
	}

	loadAvailableLang() {
		this._langService.getAvailableLang().then((langs) => {
			langs.forEach((lang) => {
				this.langs = lang;
			});
		});
	}

	selectLang(lang: string) {
		console.log(lang);
		this._route.navigate([lang + '/' + this.actualOrder]);
	}

	/**Obtenemos las etiquetas dependiendo el lenguaje seleccionado. */
	loadLabelsByLang(lang: string) {
		this._labelsService.getLabels().then((labelsSnapshot) => {
			labelsSnapshot.forEach((labels) => {
				labels.forEach((label) => {
					this.labels[label.id] = label[lang];
				});
			});
		});
	}

	signIn(user, password) {
		this.auth.signInWithEmailAndPassword(user, password).then((firebaseAuth) => {
			user = firebaseAuth.user;
			this.snotifyService.success(this.labels['admin.signin.msg.success']);
		}).catch(e => {
			console.log(e);
			this.snotifyService.error(e.message, 'Error');
		});
	}

	signOut() {
		this.user = null;
		this.auth.signOut();
		this.snotifyService.success(this.labels['admin.signout.button']);
	}

	getSettingsConfig() {
		return this.afs.collection(this.settingsCollectionName).snapshotChanges().pipe(
			map(actions => actions.map(a => {
				const data = a.payload.doc.data() as Setting;
				const id = a.payload.doc.id;
				return { id, ...data };
			})));
	}

	updateSettingPath(settingUid:string, path:string) {
		const data = {
			path: path
		};

		return this.afs.collection(this.settingsCollectionName).doc(settingUid).set(data, { merge: true });
	}

	getDistanceConfig() {
		return this.afs.collection(this.distanceCollectionName, ref => ref.orderBy('amount', 'asc')).snapshotChanges().pipe(
			map(actions => actions.map(a => {
				const data = a.payload.doc.data() as DistanceAmount;
				const id = a.payload.doc.id;
				return { id, ...data };
			})));
	}

	upsertDistanceAmount(distance: DistanceAmount) {

		const data = {
			amount: distance.amount,
			label: distance.label
		};

		return this.afs.collection(this.distanceCollectionName).doc(distance.id).set(data, { merge: true });
	}

}
