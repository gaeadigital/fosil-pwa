import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Lang } from '../entity/lang';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LangService {

  langCollectionName = 'language';

  constructor(private afs: AngularFirestore) { }

  async getAvailableLang(){
    return await this.afs.collection<Lang>(this.langCollectionName).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Lang;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }
}
