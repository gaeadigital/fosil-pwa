import { Ingredient } from './ingredient';
export class MenuItem {
	id?: string = null;
	orderId: string = null;
	label: string = null;
	amount: number = null;
	details: string = null;
	ingredients?: Array<Ingredient>;
}
