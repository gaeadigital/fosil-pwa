export class Lang {

    id?:string = null;
    country:string = null;
    icon:string = null;
    lang:string = null;
    locale:string = null;

}
