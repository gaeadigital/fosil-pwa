export class Order {
	id?: string = null;
	address: string = null;
	phoneNumber: string = null;
	paymentType: string = null;
	paymentStatus: string = null;
	status: string = null;
	totalAmount: number = null;
	deliveryAmount: number = null;
	timestampReceived: Date = null;
	timestampRecord: Date = null;
	customerPickUp?:boolean = false;
	to?:string = null;
	cc?:string = null;

	order?:any;
	isCanceled?:boolean = false;
}
