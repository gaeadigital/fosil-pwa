export class OrderHistory {
    id?:string = null;
    orderId:string= null;
    recordDate:Date= null;
    details:string = null;
}
