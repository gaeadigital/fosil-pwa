import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Order } from '../entity/order';
import { MenuItem } from '../entity/menu-item';
import { map } from 'rxjs/operators';
import { Utils } from '../utils/utils';
import { OrderHistory } from '../entity/order-history';

@Injectable({
	providedIn: 'root'
})
export class OrderService {
	orderCollectionName = 'orders';
	orderItemsCollectionName = 'order-items';
	orderHistoryCollectionName = 'order-history';

	constructor(private afs: AngularFirestore) { }

	getId() {
		return this.afs.createId();
	}



	async getOrderById(orderId: string) {
		return await this.afs.doc(this.orderCollectionName + '/' + orderId).snapshotChanges().pipe(
			map((action) => {
				const data = action.payload.data() as Order;
				const id = action.payload.id;
				return { id, ...data };
			})
		);
	}

	getOrdersReceivedNStatus(status: string, startDate:Date, endDate:Date) {
		return this.afs
			.collection<Order>(this.orderCollectionName, (ref) => 
				ref.where('status','==',status)
				.orderBy('timestampRecord', 'asc').startAfter(startDate).endBefore(endDate))
			.snapshotChanges()
			.pipe(
				map((actions) =>
					actions.map((a) => {
						const data = a.payload.doc.data() as Order;
						data.timestampRecord = Utils.timestampToDate(data.timestampRecord);
						if(data.customerPickUp === undefined){
							data.customerPickUp = false;
						}
						const id = a.payload.doc.id;
						return { id, ...data };
					})
				)
			);
	}

	upsertOrder(order: Order) {
		const data = {
			address: order.address,
			phoneNumber: order.phoneNumber,
			paymentType: order.paymentType,
			paymentStatus: order.paymentStatus,
			status: order.status,
			totalAmount: order.totalAmount,
			deliveryAmount: order.deliveryAmount,
			timestampReceived: order.timestampReceived,
			timestampRecord: order.timestampRecord
		};
		return this.afs.doc<Order>(this.orderCollectionName + '/' + order.id).set(data, { merge: true });
	}

	upsertOrderStatus(order: Order) {
		const data = {
			status: order.status,
		};
		return this.afs.doc(this.orderCollectionName + '/' + order.id).set(data, { merge: true });
	}

	upsertOrderDelivered(order: Order) {
		const data = {
			status: order.status,
			address: order.address,
			phoneNumber: order.phoneNumber,
		};
		return this.afs.doc(this.orderCollectionName + '/' + order.id).set(data, { merge: true });
	}

	upsertMenuItem(menuItem: MenuItem) {
		const data = {
			orderId: menuItem.orderId,
			label: menuItem.label,
			amount: menuItem.amount,
			details: menuItem.details
		};

		return this.afs.doc<MenuItem>(this.orderItemsCollectionName + '/' + menuItem.id).set(data, { merge: true });
	}

	async getItemsByOrder(orderId: string) {
		return await this.afs
			.collection<MenuItem>(this.orderItemsCollectionName, (ref) => ref.where('orderId', '==', orderId))
			.snapshotChanges()
			.pipe(
				map((actions) =>
					actions.map((a) => {
						const data = a.payload.doc.data() as MenuItem;
						const id = a.payload.doc.id;
						return { id, ...data };
					})
				)
			);
	}


	// Order history

	async getHistoryOrder(orderId:string) {
		return await this.afs
		.collection<OrderHistory>(this.orderHistoryCollectionName, (ref) => ref.where('orderId', '==', orderId))
		.snapshotChanges()
		.pipe(
			map((actions) =>
				actions.map((a) => {
					const data = a.payload.doc.data() as OrderHistory;
					const id = a.payload.doc.id;
					return { id, ...data };
				})
			)
		);
	}

	upsertHistory(orderHistory:OrderHistory){

		if(orderHistory.id === null){
			orderHistory.id = this.afs.createId();
		}

		const data = {
			orderId: orderHistory.orderId,
			details: orderHistory.details,
			recordDate: orderHistory.recordDate
		};

		return this.afs.doc(this.orderHistoryCollectionName + '/' + orderHistory.id).set(data, { merge: true });
	}
}
