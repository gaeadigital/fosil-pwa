import { TestBed } from '@angular/core/testing';

import { GroupIngredientService } from './group-ingredient.service';

describe('GroupIngredientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupIngredientService = TestBed.get(GroupIngredientService);
    expect(service).toBeTruthy();
  });
});
