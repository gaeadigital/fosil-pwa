import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { GroupIngredient } from '../entity/group-ingredient';
import { map } from 'rxjs/operators';
import { Ingredient } from '../entity/ingredient';

@Injectable({
  providedIn: 'root'
})
export class GroupIngredientService {
  groupCollectionName = 'group-ingredient';
  ingredientsCollectionName = 'ingredients';

  constructor(private afs: AngularFirestore) { }

   getGroups() {
    return this.afs
      .collection<GroupIngredient>(this.groupCollectionName, (ref) => ref.orderBy('order', 'asc'))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as GroupIngredient;
            data.ingredients = this.getIngredientsBygroup(a.payload.doc.id);
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  getIngredientsBygroup(group: string) {
    return this.afs
      .collection<Ingredient>(this.ingredientsCollectionName, (ref) => ref.where('group', '==', group).orderBy('order', 'asc'))
      .snapshotChanges()
      .pipe(
        map((actions) =>
          actions.map((a) => {
            const data = a.payload.doc.data() as Ingredient;
            if(!data.check){
              data.check = false;
            }
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  updatePrice(ingredientId: string, amount: number) {
    const data = {
      amount: amount
    };
    return this.afs.doc(this.ingredientsCollectionName + '/' + ingredientId).set(data, { merge: true });
  }

  updatePreselected(ingredientId: string, check: boolean) {
    const data = {
      check: check
    };
    return this.afs.doc(this.ingredientsCollectionName + '/' + ingredientId).set(data, { merge: true });
  }

  updateVisible(ingredientId: string, visible:boolean) {
    const data = {
      visible:visible
    };
    return this.afs.doc(this.ingredientsCollectionName + '/' + ingredientId).set(data, { merge: true });
  }

  upsert(ingredient:Ingredient){
    if(ingredient.id === null){
      ingredient.id = this.afs.createId();
      console.log(ingredient.id);
    }

    const data = {
      amount:ingredient.amount,
      check:ingredient.check,
      group:ingredient.group,
      label:ingredient.label,
      order:ingredient.order,
      visible:ingredient.visible
    };

		return this.afs.doc(this.ingredientsCollectionName + '/' + ingredient.id).set(data, { merge: true });
  }
  
  upsertGroup(group:GroupIngredient){

    const data = {
      detail:group.detail,
      label:group.label,
      order:group.order,
      visible:group.visible
    };

		return this.afs.doc(this.groupCollectionName + '/' + group.id).set(data, { merge: true });

  }

  deleteIngrediente(ingredientId:string){
    return this.afs.doc(this.ingredientsCollectionName + '/' + ingredientId).delete();
  }

  deleteGroup(groupId:string){
    return this.afs.doc(this.groupCollectionName + '/' + groupId).delete();
  }
}
