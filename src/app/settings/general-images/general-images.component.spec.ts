import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralImagesComponent } from './general-images.component';

describe('GeneralImagesComponent', () => {
  let component: GeneralImagesComponent;
  let fixture: ComponentFixture<GeneralImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
