import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../commons/settings.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Setting } from '../../entity/setting';
import { MatDialog } from '@angular/material';
import { UploadDialogComponent } from './upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-general-images',
  templateUrl: './general-images.component.html',
  styleUrls: ['./general-images.component.scss']
})
export class GeneralImagesComponent implements OnInit {

  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;

  settings: Array<Setting>;
  displayedColumns: string[] = ['id','label', 'path'];

  constructor(public settingsService: SettingsService,private storage: AngularFireStorage,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.settingsService.getSettingsConfig().subscribe(result => this.settings = result);
  }

  uploadFile(event, setting) {
    console.log(event);
    const file = event.target.files[0];
    const filePath = '/settings/background/'+file.name;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    //observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          fileRef.getDownloadURL().subscribe(result => {
            console.log(result);
          });
          
        } )
     )
    .subscribe()
  }

  showUploadDialog(setting: Setting) {
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      data: {
        setting: setting
      },
      width:  '50%'
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }
  
}
