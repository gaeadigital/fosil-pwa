import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Setting } from '../../../entity/setting';
import { SettingsService } from '../../../commons/settings.service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {

  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;

  setting2update: Setting;
  fileName:string;

  constructor(private storage: AngularFireStorage,@Inject(MAT_DIALOG_DATA) public data: any, 
  public dialog: MatDialog, public settingsService:SettingsService) {
    this.setting2update = new Setting();
   }

  ngOnInit() {
    if(this.data){
      this.setting2update = this.data.setting;
    } else {
      this.dialog.closeAll();
    }
  }

  uploadFile(event) {
    console.log(event);
    const file = event.target.files[0];
    this.fileName = file.name;
    const filePath = '/settings/background/'+file.name;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    //observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          fileRef.getDownloadURL().subscribe(result => {
            console.log(result);
            this.settingsService.updateSettingPath(this.setting2update.id, result).then(success => {
              this.dialog.closeAll();
            });
          });
          
        } )
     )
    .subscribe()
  }

}
