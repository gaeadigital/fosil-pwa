import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../commons/settings.service';
import { MatSnackBar } from '@angular/material';
import { DistanceAmount } from 'src/app/entity/distance-amount';

@Component({
  selector: 'app-distance',
  templateUrl: './distance.component.html',
  styleUrls: ['./distance.component.scss']
})
export class DistanceComponent implements OnInit {

  distances: Array<DistanceAmount>;
  displayedColumns: string[] = ['id','label', 'amount'];

  constructor(public settingsService: SettingsService,
    private _snackBar: MatSnackBar, ) { }

  ngOnInit() {
    this.distances = new Array<DistanceAmount>();
    this.settingsService.getDistanceConfig().subscribe(result => this.distances = result);
  }

  updateAmount(distance){
    console.log(distance);
    this.settingsService.upsertDistanceAmount(distance).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.distances.updated']);
    }).catch(err => {
      console.error(err);
    });
  }


  showSnackbar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }


}
