import { firestore } from 'firebase';

export class Utils {
	static timestampToDate(date2Change) {
		const timeStemp = date2Change as firestore.Timestamp;
		const date = timeStemp.toDate();
		return date;
	}

	static dateString(date2Parse: Date): string {
		return date2Parse.getFullYear() + '-' + date2Parse.getMonth() + '-' + date2Parse.getDate();
	}

	static toJsonDate(date2Parse: Date) {
		return {
			y: date2Parse.getFullYear(),
			M: date2Parse.getMonth(),
			d: date2Parse.getDate(),
			h: 0,
			m: 0,
			s: 0,
			ms: 0
		};
	}

	static formatDate(date2Format: Date) {
		return date2Format.getDate() + '/' + (date2Format.getMonth() + 1) + '/' + date2Format.getFullYear();
	}

	static fixStartDate(date: Date) {
		if (date.toString().length > 10) {
			return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
		} else {
			const splitDate = date.toString().split('-');
			const year = parseInt(splitDate[0], 10);
			const month = parseInt(splitDate[1], 10) - 1;
			const day = parseInt(splitDate[2], 10);
			return new Date(year, month, day, 0, 0, 0);
		}
	}

	static fixEndDate(date: Date) {
		if (date.toString().length > 10) {
			return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 11, 59, 59);
		} else {
			const splitDate = date.toString().split('-');
			const year = parseInt(splitDate[0], 10);
			const month = parseInt(splitDate[1], 10) - 1;
			const day = parseInt(splitDate[2], 10);
			return new Date(year, month, day, 11, 59, 59);
		}
	}

	static calculateNextDate(startDate: Date, next: number, metric: string) {
		if (metric === 'D') {
			return new Date(startDate.setTime(startDate.getTime() + next * 86400000));
		} else {
			return new Date(startDate.setMonth(startDate.getMonth() + next * 1));
		}
	}

	static calculateAfterDate(startDate: Date, next: number, metric: string) {
		if (metric === 'D') {
			return new Date(startDate.setTime(startDate.getTime() - next * 86400000));
		} else {
			return new Date(startDate.setMonth(startDate.getMonth() - next * 1));
		}
	}

}
