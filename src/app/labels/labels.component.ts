import { Component, OnInit, HostListener } from '@angular/core';
import { SettingsService } from '../commons/settings.service';
import { Label } from '../entity/label';
import { LabelsService } from '../commons/labels.service';
import { Lang } from '../entity/lang';
import { LangService } from '../commons/lang.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSnackBar } from '@angular/material';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.scss']
})
export class LabelsComponent implements OnInit {

  labelsFilter: Array<Label>;
  labels: Array<Label>;
  langs:Array<Lang>;

  labelId2Search:string;

  isSmallScreen:boolean;

  constructor(public settingsService: SettingsService,
    private _labelsService: LabelsService, 
    private _langService:LangService,private breakpointObserver: BreakpointObserver,
    private _snackBar: MatSnackBar, private auth:AngularFireAuth) {
    this.labels = new Array<Label>();
    this.langs = new Array<Lang>();
    this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
	}

	@HostListener('window:resize', [ '$event' ])
	onResize(event) {
		this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
	}


  ngOnInit() {
    this._langService.getAvailableLang().then((langSnapshot) => {
      this.langs = new Array<Lang>();
      langSnapshot.forEach((langCollection) => {
        this.langs = langCollection;
      })
    });
    //Obtenemos las etiquetas.
    this._labelsService.getLabels().then((labelsSnapshot) => {
      this.labels = new Array<Label>();
      this.labelsFilter = new Array<Label>();
			labelsSnapshot.forEach((labels) => {
        labels.forEach(label => {
          this.labels.push(label)
          this.labelsFilter.push(label);
        });
      });
    });
    this.auth.authState.subscribe((user) => {
      this.settingsService.user;
    });
  }

  search(){
    this.labelsFilter = new Array<Label>();
    this.labels.forEach(label => {
      if(label.id.includes(this.labelId2Search)){
        this.labelsFilter.push(label);
      }
    });
    console.log(this.labelsFilter.length);
  }

  updateLabel(label){
    console.log(label);
    const labelId = label['id'];
    delete label['id']
    this._labelsService.upsertLabel(labelId, label).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.labels.successupdate']);
      this.ngOnInit();
    }).catch(e=> {
      console.error(e);
    });
  }

  toggleDeleteLabel(label){
    if(label.confirmDelete){
      label.confirmDelete = false;
    } else {
      label.confirmDelete = true;
    }
  }

  confirmDelete(label){
    this._labelsService.deleteLabel(label.id);
    this.ngOnInit();
  }


  showSnackbar(message:string){
    this._snackBar.open(message,'', {
      duration: 2000
    });
  }
}
