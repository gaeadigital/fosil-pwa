import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule, BUCKET  } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { LabelsService } from './commons/labels.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatProgressBarModule, MatNativeDateModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { OrderComponent } from './order/order.component';
import { LangService } from './commons/lang.service';
import { FormsModule, ReactiveFormsModule } from '../../node_modules/@angular/forms';
import { PrepareMenuItemComponent } from './prepare-menu-item/prepare-menu-item.component';
import { SettingsService } from './commons/settings.service';
import { HomeComponent } from './home/home.component';
import { OrderDetailComponent } from './order/order-detail/order-detail.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { LabelsComponent } from './labels/labels.component';
import { CloudMessagingService } from './commons/cloud-messaging.service';
import { LoginComponent } from './home/login/login.component';
import { SettingsComponent } from './settings/settings.component';
import { DetailIngredientComponent } from './ingredients/detail-ingredient/detail-ingredient.component';
import { GroupDetailComponent } from './ingredients/group-detail/group-detail.component';
import { DistanceComponent } from './settings/distance/distance.component';
import { GeneralImagesComponent } from './settings/general-images/general-images.component';
import { UploadDialogComponent } from './settings/general-images/upload-dialog/upload-dialog.component';
import { OrderHistoryComponent } from './order/order-history/order-history.component';


@NgModule({
	declarations: [ AppComponent, OrderComponent, PrepareMenuItemComponent, HomeComponent, OrderDetailComponent, IngredientsComponent, LabelsComponent, LoginComponent, SettingsComponent, DetailIngredientComponent, GroupDetailComponent, DistanceComponent, GeneralImagesComponent, UploadDialogComponent, OrderHistoryComponent ],
	imports: [
		BrowserModule,
		AppRoutingModule,
		AngularFireModule.initializeApp(environment.firebase),
		AngularFirestoreModule,
		AngularFireAuthModule,
		AngularFireMessagingModule,
		AngularFireStorageModule,
		BrowserAnimationsModule,
		SnotifyModule,
		LayoutModule,
		FormsModule,
		ReactiveFormsModule,
		MatToolbarModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatButtonModule,
		MatListModule,
		MatCardModule,
		MatDividerModule,
		MatGridListModule,
		MatIconModule,
		MatSidenavModule,
		MatTabsModule,
		MatTableModule,
		MatPaginatorModule,
		MatDialogModule,
		MatStepperModule,
		MatExpansionModule,
		MatSnackBarModule,
		MatChipsModule,
		MatMenuModule,
		MatSlideToggleModule,
		MatCheckboxModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatNativeDateModule,
		
	],
	providers: [ SettingsService, LangService, LabelsService, CloudMessagingService , 
		{ provide: 'SnotifyToastConfig', useValue: ToastDefaults},
		{ provide: BUCKET, useValue: 'fosil-pwa-test.appspot.com' },
    	SnotifyService],
	bootstrap: [ AppComponent ],
	entryComponents:[
		OrderDetailComponent,LoginComponent,DetailIngredientComponent, GroupDetailComponent, UploadDialogComponent
	]
})
export class AppModule {}
