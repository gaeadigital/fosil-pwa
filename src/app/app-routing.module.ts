import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './order/order.component';
import { PrepareMenuItemComponent } from './prepare-menu-item/prepare-menu-item.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
	{ path: ':lang', component: HomeComponent },
	{ path: '',   redirectTo: '/cs-CZ', pathMatch: 'full' }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {}
