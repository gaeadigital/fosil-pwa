import { Component, OnInit, HostListener } from '@angular/core';
import { GroupIngredient } from '../entity/group-ingredient';
import { GroupIngredientService } from '../menu/group-ingredient.service';
import { Ingredient } from '../entity/ingredient';
import { SettingsService } from '../commons/settings.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { BreakpointObserver } from '@angular/cdk/layout';
import { AngularFireAuth } from '@angular/fire/auth';
import { DetailIngredientComponent } from './detail-ingredient/detail-ingredient.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { Observable } from 'rxjs';
import { last } from 'rxjs/operators';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements OnInit {

  groupIngredients: Observable<GroupIngredient[]>;
  displayedColumns: string[] = ['value', 'amount', 'check', 'accions'];

  isSmallScreen: boolean;

  constructor(public settingsService: SettingsService,
    private _snackBar: MatSnackBar,
    private _groupIngredientService: GroupIngredientService,
    private breakpointObserver: BreakpointObserver,
    private auth: AngularFireAuth,
    public dialog: MatDialog) {
    this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
  }

  ngOnInit() {
    this.groupIngredients = this._groupIngredientService.getGroups();
    this.auth.authState.subscribe((user) => {
      this.settingsService.user = user;
    });
  }

  updatePrice(ingredient: Ingredient) {
    console.log(ingredient);
    this._groupIngredientService.updatePrice(ingredient.id, ingredient.amount).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }

  selected(ingredient: Ingredient) {
    if (ingredient.check) {
      ingredient.check = false;
    } else {
      ingredient.check = true;
    }
    console.log(ingredient);

    this._groupIngredientService.updatePreselected(ingredient.id, ingredient.check).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }

  toogleVisibile(ingredient: Ingredient) {
    if (ingredient.visible) {
      ingredient.visible = false;
    } else {
      ingredient.visible = true;
    }
    console.log(ingredient);
    this._groupIngredientService.updateVisible(ingredient.id, ingredient.visible).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }

  hideIngredient(ingredientId:string){
    console.log(ingredientId);
    this._groupIngredientService.updateVisible(ingredientId, false).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }

  showIngredient(ingredientId:string){
    console.log(ingredientId);
    this._groupIngredientService.updateVisible(ingredientId, true).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }


  showSnackbar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }

  addIngredient() {
    console.log('nuevo ingrediente');
  }

  showDetail(grouo: GroupIngredient) {
    const dialogRef = this.dialog.open(DetailIngredientComponent, {
      data: {
        group: grouo
      },
      width: (this.isSmallScreen) ? '95%' : '50%'
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }

  addGroup() {
    this.showGroupDetail(new GroupIngredient());
  }

  showGroupDetail(group: GroupIngredient) {
    let length = 0;
    let lastOrder = null;
    this.groupIngredients.subscribe(result => {
      length = result.length
      lastOrder = result[length - 1];

      group.order = lastOrder;
      const dialogRef = this.dialog.open(GroupDetailComponent, {
        data: {
          group: group,
          size: lastOrder.order + 1
        },
        width: (this.isSmallScreen) ? '95%' : '40%'
      });


      dialogRef.afterClosed().subscribe((result) => {
        console.log('The dialog was closed');
      });

    });
  }

  deleteGroup(group) {
    if (group.confirmDelete) {
      group.confirmDelete = false;
    } else {
      group.confirmDelete = true;
    }
  }

  confirmDelete(group) {
    this._groupIngredientService.deleteGroup(group.id).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }

  deleteIngredient(ingredient) {
    if (ingredient.confirmDelete) {
      ingredient.confirmDelete = false;
    } else {
      ingredient.confirmDelete = true;
    }
  }

  confirmDeleteIngredient(ingredient) {
    this._groupIngredientService.deleteIngrediente(ingredient.id).then(success => {
      this.showSnackbar(this.settingsService.labels['admin.ingredients.updateamount']);
    }).catch(err => {
      console.error(err);
    });
  }

}
