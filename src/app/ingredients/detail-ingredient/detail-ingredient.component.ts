import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { LangService } from '../../commons/lang.service';
import { LabelsService } from '../../commons/labels.service';
import { Label } from '../../entity/label';
import { Lang } from '../../entity/lang';
import { SettingsService } from '../../commons/settings.service';
import { Ingredient } from '../../entity/ingredient';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialog } from '@angular/material';

import { GroupIngredient } from '../../entity/group-ingredient';
import { GroupIngredientService } from '../../menu/group-ingredient.service';

@Component({
  selector: 'app-detail-ingredient',
  templateUrl: './detail-ingredient.component.html',
  styleUrls: ['./detail-ingredient.component.scss']
})
export class DetailIngredientComponent implements OnInit {

  label:Label;
  baseLabel:string;

  ingredient:Ingredient;
  langs:Array<Lang>;

  group:GroupIngredient;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public matDialog:MatDialog, private _snackBar: MatSnackBar,
  public settingsService: SettingsService, private _langService: LangService,
   private _labelsService: LabelsService, private _groupIngredientService:GroupIngredientService) {
    this.label = new Label();
    
    this.ingredient = new Ingredient();
    if (this.data) {
      this.group = this.data.group;
      
      this.group.ingredients.subscribe(result=>{
        this.ingredient.order = result.length;
      });
      this.baseLabel='menu.ingredient.'+this.group.id+'.';
    } else {
      this.group = new GroupIngredient();
    }
   }

  ngOnInit() {
    this._langService.getAvailableLang().then((langSnapshot) => {
      this.langs = new Array<Lang>();
      langSnapshot.forEach((langCollection) => {
        this.langs = langCollection;
      })
    });
  }

  open2Edit(ing:Ingredient){
    this.ingredient = ing;
  }

  upsert(label){
    console.log(this.label);
    if(this.label.id === '' || this.label.id === null){
      this.showSnackbar(this.settingsService.labels['admin.required.fields']);
      return;
    }else {
      const compLabelId = this.baseLabel+this.label.id;
      this.ingredient.group = this.group.id;
      this.ingredient.label = compLabelId;
      const labelId = label['id'];
      delete label['id'];
      delete label['confirmDelete'];
      delete label['data'];
      console.log(this.label);
      console.log(this.ingredient);
      console.log(this.buildMap(label));
      this._labelsService.upsertLabel(compLabelId,this.buildMap(label)).then(success => {
        this._groupIngredientService.upsert(this.ingredient).then(success => {
          this.showSnackbar(this.settingsService.labels['admin.labels.successupdate']);
          this.matDialog.closeAll();
        }).catch(e=> {
          console.error(e);
        });
      }).catch(e=> {
        console.error(e);
      });
    }
  }


  showSnackbar(message:string){
    this._snackBar.open(message,'', {
      duration: 2000
    });
  }

  buildMap(obj) {
    let map = {};
    Object.keys(obj).forEach(key => {
        map[key]= obj[key];
    });
    return map;
  };
}
