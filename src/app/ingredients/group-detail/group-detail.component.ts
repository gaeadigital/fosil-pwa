import { Component, OnInit, Inject } from '@angular/core';
import { SettingsService } from '../../commons/settings.service';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { GroupIngredientService } from '../../menu/group-ingredient.service';
import { GroupIngredient } from '../../entity/group-ingredient';
import { Label } from '../../entity/label';
import { Lang } from '../../entity/lang';
import { LangService } from '../../commons/lang.service';
import { LabelsService } from '../../commons/labels.service';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.scss']
})
export class GroupDetailComponent implements OnInit {

  label: Label;
  group: GroupIngredient;
  sizeIdx: number;
  langs: Array<Lang>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public settingsService: SettingsService,
    public matDialog:MatDialog,
    private _snackBar: MatSnackBar,
    private _langService: LangService,
   private _labelsService: LabelsService, private _groupIngredientService:GroupIngredientService) {
    this.label = new Label();
    this.label.id = 'menu.group.ingredient.'
    if (this.data) {
      this.group = this.data.group;
      this.sizeIdx = this.data.size;
      this.group.order = this.data.size;
    } else {
      this.group = new GroupIngredient();
    }
  }

  ngOnInit() {
    this._langService.getAvailableLang().then((langSnapshot) => {
      this.langs = new Array<Lang>();
      langSnapshot.forEach((langCollection) => {
        this.langs = langCollection;
      })
    });
  }

  upsertGroup() {
    console.log(this.group);
    if(this.group.id===''|| this.group.id === null){
      this.showSnackbar(this.settingsService.labels['admin.required.fields']);
      return;
    } else {
      const labelId = this.label['id'];
      delete this.label['id'];
      console.log(this.label);
      console.log(this.group);
      this.group.visible = true;
      this.group.label = labelId;
      console.log(this.buildMap(this.label));
      this._labelsService.upsertLabel(labelId,this.buildMap(this.label)).then(success => {
        this._groupIngredientService.upsertGroup(this.group).then(success => {
          this.showSnackbar(this.settingsService.labels['admin.group.successupdate']);
          this.matDialog.closeAll();
        }).catch(e=> {
          console.error(e);
        });
      }).catch(e=> {
        console.error(e);
      });
    }
  }


  showSnackbar(message:string){
    this._snackBar.open(message,'', {
      duration: 2000
    });
  }

  buildMap(obj) {
    let map = {};
    Object.keys(obj).forEach(key => {
        map[key]= obj[key];
    });
    return map;
  };
}
