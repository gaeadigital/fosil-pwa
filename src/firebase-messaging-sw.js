importScripts('https://www.gstatic.com/firebasejs/5.3.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.3.1/firebase-messaging.js');

/** PRODUCTION*/
firebase.initializeApp({
    messagingSenderId: "39969959389"
}); 

/** TEST   
firebase.initializeApp({
    messagingSenderId: "764162782811"
});*/

const messaging = firebase.messaging();