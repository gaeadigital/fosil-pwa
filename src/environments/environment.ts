// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCuvotkszfsyv99NOeb4pNYu5bDfjaw_Ww",
    authDomain: "fosil-pwa-test.firebaseapp.com",
    databaseURL: "https://fosil-pwa-test.firebaseio.com",
    projectId: "fosil-pwa-test",
    storageBucket: "fosil-pwa-test.appspot.com",
    messagingSenderId: "764162782811",
    appId: "1:764162782811:web:c274e49c5ced05d161f17e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
